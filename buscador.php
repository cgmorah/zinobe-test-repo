<?php
session_start();
if(!isset($_SESSION["nombre"])) {
    header("location:index.php");
} else {
    require_once("conexion.php");
    require_once("header.html");
?>

    <?php
    $datos = "";
    if(isset($_POST["search"]))
    {
        $consulta = $_POST['query'];
        // obtiene el valor enviado en el formulario de búsqueda

        $min_length = 3;
        // puede establecer la longitud mínima de la consulta si lo desea

        if(strlen($consulta) >= $min_length){ // if query length is more or equal minimum length then

            $consulta = htmlspecialchars($consulta);
            // changes characters used in html to their equivalents, for example: < to &gt;

            $sql = "SELECT * FROM usuarios
                WHERE (`nombre` LIKE '%".$consulta."%') OR (`email` LIKE '%".$consulta."%')";
            $result=$mysqli->query($sql);
            $rows = $result->num_rows;

            if($rows > 0){
                $datos .= "<h4>Resultados</h4>
                        <table>
                          <tr>
                            <th>ID</th>
                            <th>Correo</th>
                            <th>Nombre completo</th>
                            <th>Pais</th>
                          </tr>";
                while($row = $result->fetch_assoc()){
                    $datos .= "  <tr>
                                    <td>".$row['id']."</td>
                                    <td>".$row['email']."</td>
                                    <td>".$row['nombre']."</td>
                                    <td>".$row['pais']."</td>
                                  </tr>";
                }
                $datos .= "</table>";
            }
            else{
                $datos .=  "No results";
            }

        }
        else{ // if query length is less than minimum
            $message = "La longitud mínima para buscar es de ".$min_length;
            $classe = "error";
        }
    }
}
    ?>

    <div class="container mlogin">
        <h2>Bienvenido, <span><?php echo $_SESSION["nombre"];?>! </span></h2>
        <p><a href="logout.php">Finalice</a> sesión aquí!</p>

        <h3>Buscar Usuarios</h3>
        <form  method="post" action="buscador.php"  id="searchform">
            <p>Puede buscar por nombre o correo</p>
            <p>
                <input type="text" name="query" id="query" class="input" value="" size="20" required placeholder="Buscar Usuarios"/></label>
            </p>
            <p class="submit">
                <input type="submit" name="search" class="button" value="Buscar" />
            </p></form><br />
        </form><br /><br />
        <div>
            <?php echo $datos; ?>
        </div>
        <?php if (!empty($message)) {echo "<p class=\"".$classe."\">". $message . "</p>";} ?>
    </div>

<?php include("footer.html"); ?>
