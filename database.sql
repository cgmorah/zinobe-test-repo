/* Crear Base Datos */
CREATE DATABASE usuarioszinobe;
/* Create Table */
CREATE TABLE `usuarios` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(32) COLLATE utf8_spanish_ci NOT NULL,
  `email` VARCHAR(32) COLLATE utf8_spanish_ci NOT NULL,
  `pais` VARCHAR(32) COLLATE utf8_spanish_ci NOT NULL,
  `password` VARCHAR(32) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci
