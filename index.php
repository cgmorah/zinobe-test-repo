<?php
/**
 * Created by PhpStorm.
 * User: cgmorah
 * Date: 07/05/2018
 */
session_start();

if(isset($_SESSION["nombre"])) {
    header("location:buscador.php");
}

require_once("conexion.php");
require_once("header.html");

if(isset($_SESSION["session_username"])){
// echo "Session is set"; // for testing purposes
    header("Location: buscador.php");
}

if(isset($_POST["login"]))

    {
        $email = mysqli_real_escape_string($mysqli,$_POST['email']);
        $password = mysqli_real_escape_string($mysqli,$_POST['password']);

        $md5_pass = md5($password);

        $sql = "SELECT nombre FROM usuarios WHERE email = '$email' AND password = '$md5_pass'";
        $result=$mysqli->query($sql);
        $rows = $result->num_rows;
        if($rows > 0) {
            $row = $result->fetch_assoc();
            $_SESSION['nombre'] = $row['nombre'];

            header("location: buscador.php");
        } else {
            $message = "El nombre o contraseña son incorrectos!";
            $classe = "error";
        }
    }


if(isset($_GET["succes"]))
{
    $message = "Cuenta creada correctamente!";
    $classe = "succes";
}
?>

    <div class="container mlogin">
        <div id="login">
            <h1>Iniciar sesión</h1>
            <form name="loginform" id="loginform" action="index.php" method="POST">
                <p>
                    <label for="email">Email<br />
                        <input type="email" name="email" id="email" class="input" value="" size="20" required/></label>
                </p>
                <p>
                    <label for="password">Contraseña<br />
                        <input type="password" name="password" id="password" class="input" value="" size="20" minlength="6" required/></label>
                </p>
                <p class="submit">
                    <input type="submit" name="login" class="button" value="Entrar" />
                </p></form><br />
                <p class="regtext">No estas registrado? <a href="registro.php" >Registrate Aquí</a>!</p>
            </form>

        </div>
        <?php if (!empty($message)) {echo "<p class=\"".$classe."\">". $message . "</p>";} ?>
    </div>

<script type="text/javascript">
    var email = document.getElementById("email");

    email.addEventListener("input", function (event) {
        if (email.validity.typeMismatch) {
            email.setCustomValidity("no es un E-mail valido!");
        } else {
            email.setCustomValidity("");
        }
    });
</script>

<?php include("footer.html"); ?>
