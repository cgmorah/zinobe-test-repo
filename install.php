<?php
/**
 * Created by PhpStorm.
 * User: @cgmorah
 * Date: 08/05/2018
 */
require_once("header.html");

if(isset($_POST["install"])) {

    if($_POST["host"] && $_POST["username"]) {
        class Crear_database
        {

            protected $pdo;


            public function __construct()
            {
                $this->localhost = $_POST["host"];
                $this->username = $_POST["username"];
                $this->password = $_POST["password"];

                $this->pdo = new PDO("mysql:host=$this->localhost;", "$this->username", "$this->password");
            }

            //creamos la base de datos y las tablas que necesitemos
            public function my_db()
            {
                //creamos la base de datos si no existe
                $crear_db = $this->pdo->prepare('CREATE DATABASE IF NOT EXISTS usuarioszinobe COLLATE utf8_spanish_ci');
                $crear_db->execute();

                //decimos que queremos usar la tabla que acabamos de crear
                if ($crear_db):
                    $use_db = $this->pdo->prepare('USE usuarioszinobe');
                    $use_db->execute();
                endif;

                //si se ha creado la base de datos y estamos en uso de ella creamos las tablas
                if ($use_db):

                    //creamos la tabla
                    $crear_tb_users = $this->pdo->prepare('
                        CREATE TABLE `usuarios` (
                          `id` INT(11) NOT NULL AUTO_INCREMENT,
                          `nombre` VARCHAR(32) COLLATE utf8_spanish_ci NOT NULL,
                          `email` VARCHAR(32) COLLATE utf8_spanish_ci NOT NULL,
                          `pais` VARCHAR(32) COLLATE utf8_spanish_ci NOT NULL,
                          `password` VARCHAR(32) COLLATE utf8_spanish_ci NOT NULL,
                          PRIMARY KEY (`id`),
                          UNIQUE KEY `email` (`email`)
                        ) ENGINE=MYISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci
                    ');

                    $crear_tb_users->execute();

                endif;

            }
        }

        try {
            //crear bd
            $db = new Crear_database();
            $db->my_db();
            header('Location: registro.php');
        } catch (PDOException $e) {
            $message = "Falló la conexión: " . $e->getMessage();
        }


    }else{
        $message = "Los campos de Host y Contraseña son requeridos!";
    }

}
?>

<div class="container minstall">
    <div id="install">
        <h1>¡Instalemoslo juntos!</h1>
        <form name="installform" id="installform" action="" method="POST">
            <p>
                <label for="user_install">Host<br />
                    <input type="text" name="host" id="host" class="input" value="" size="20" required/></label>
            </p>
            <p>
                <label for="user_pass">Username<br />
                    <input type="text" name="username" id="username" class="input" value="" size="20" required/></label>
            </p>
            <p>
                <label for="user_pass">Contraseña<br />
                    <input type="password" name="password" id="password" class="input" value="" size="20" /></label>
            </p>
            <p class="submit">
                <input type="submit" name="install" class="button" value="Instalar" />
            </p>
        </form><br /><br />
        <p class="red">*Recuerda editar el archivo <strong>conexion.php</strong> para poder tener acceso a su base de datos</p>
    </div>
    <?php if (!empty($message)) {echo "<p class=\"error\">". $message . "</p>";} ?>
</div>

<?php include("footer.html"); ?>
